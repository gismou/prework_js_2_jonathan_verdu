function ruedas(diametro){
    if(diametro <= 10) console.log("Es una rueda para un juguete pequeño");
    if(diametro > 10 && diametro < 20) console.log("Es una rueda de un juguete mediano");
    if(diametro >= 20) console.log("Es una rueda para un juguete grande");
}

console.log("Diámetro 5 ");
ruedas(5);
console.log("Diámetro 7");
ruedas(7);
console.log("Diámetro 14");
ruedas(14);
console.log("Diámetro 31");
ruedas(31);